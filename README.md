# Quick Start
### Create db
```bash
  FLASK_APP=basic_chat.py flask initdb
```
### Start the app
```bash
  FLASK_APP=basic_chat.py flask run
```
## Authentication
Use basic auth to get a temporary token.
```bash
    curl -u admin:password -H "Accept: application/json" -H "Content-Type: application/json" -X POST http://127.0.0.1:5000/authenticate
```
`Responds with:`
```json
{
  "token": "6KXl+BT1deyXQqsI4Vhh9g9jlL+oI9UA67yAGBaceBk="
}
```
Use the returned token in subsequent requests.
```bash
  curl -H "Authorization: Bearer 6KXl+BT1deyXQqsI4Vhh9g9jlL+oI9UA67yAGBaceBk= -H "Accept: application/json"  -H "Content-Type: application/json"http://127.0.0.1:5000/messages
```
## API Endpoints
#### POST /users
Creates a user.

> Submit with this data:
```json
  {
    "username": "bryan",
    "password": "password"
  }
```
>Responds with status 201 when successful

### POST `/messages`
Creates a message.

>Submit with this data:
```json
  {
    "sender": 3,
    "recipient": 1,
    "body": "Hello"
  }
```
>Responds with status 201 when successful

### GET `/messages?users=1,2&page=1&perpage=10`
> Retrives all messages between users <br/>
> Responds with status 201 when successful

## Development
### Install dependencies
``` bash
  pip install -r requirements.txt
```
### Testing
```bash
	pytest test_routes.py
```

