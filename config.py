import os

class Config():
  BASE_DIR = os.path.abspath(os.path.dirname(__file__))
  DEBUG = True
  # Better stored in .env
  ADMINPASS="password"
  ADMINUSERNAME="admin"

  DATABASE=os.path.join(BASE_DIR, 'app/db/basic_chat.db')
  THREADS_PER_PAGE = 2

  # Enable protection agains *Cross-site Request Forgery (CSRF)*
  CSRF_ENABLED     = True

  CSRF_SESSION_KEY = "secret"

  # Secret key for signing cookies
  SECRET_KEY = "secret"
