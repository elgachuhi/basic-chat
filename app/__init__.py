from flask import Flask, g
from config import Config
import sqlite3

app = Flask(__name__)
app.config.from_object(Config)

from app import routes

@app.teardown_appcontext
def close_db(error):
  if hasattr(g, 'sqlite_db'):
    g.sqlite_db.close()

@app.errorhandler(500)
def not_found(error):
  return jsonify(message="Server down"), 500
