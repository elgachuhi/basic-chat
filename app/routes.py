from flask import request, jsonify, g, render_template
from werkzeug.security import generate_password_hash
from werkzeug.http import HTTP_STATUS_CODES
import sqlite3
import pdb
from app import app
import db_util
from auth import token_auth, basic_auth, gen_token

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/authenticate', methods=['POST'])
@basic_auth.login_required
def authenticate():
  """
    Authenticates using basic auth
    Returns user token
  """
  db = db_util.get_db()
  user = g.current_user
  user['token'] = gen_token()
  db.execute('update users set token=? where id=?',
    [user['token'], user['id']]
  )
  db.commit()
  return jsonify(token=user['token'])

@app.route('/users', methods=['POST', 'PUT'])
@token_auth.login_required
def create_user():
  data = request.get_json()
  if 'username' not in data or 'password' not in data:
    return error_response(400, 'No username or password')

  try:
    db = db_util.get_db()
    password_hash = generate_password_hash(data['password'])
    db.execute('insert into users (username, password_hash) values(?, ?)',
      [data['username'], password_hash]
    )
    db.commit()
  except sqlite3.IntegrityError:
    return error_response(400, 'username has been taken')

  return jsonify(message='user successfully created')

@app.route('/users/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_user(id):
  data = request.get_json() or {}
  if 'username' not in data and 'password' not in data:
    return error_response(400, 'must provide username or password')

  db = db_util.get_db()
  cur = db.execute('select *from users where id=?', [id])
  user = cur.fetchone()
  if not user:
    return error_response(400, 'user not found')

  db = db_util.get_db()
  password = data.get('password')
  phash = generate_password_hash(data['password']) if password else user['password_hash']
  db.execute("update users set password_hash=?, username=? where id=?",
    [phash, data.get('username') or user['username'], id]
  )
  db.commit()

  return jsonify(message='user successfully updated')

@app.route('/messages', methods=['POST'])
@token_auth.login_required
def create_message():
  db = db_util.get_db()
  data = request.get_json()

  if 'body' not in data or 'sender' not in data or 'recipient' not in data:
    return error_response(400, 'must provide body, sender and recipient')

  db.execute('insert into messages (body, sender, recipient) values(?, ?,?)',
    [data['body'], data['sender'], data['recipient']]
  )
  db.commit()

  return jsonify(message='message created successfully')

@app.route('/messages/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_message(id):
  data = request.get_json() or {}
  db = db_util.get_db()
  cur = db.execute('select *from messages where id=?', [id])
  message = cur.fetchone()
  if not message:
    return error_response(400, 'message not found')

  db = db_util.get_db()
  db.execute("update messages set sender=?, recipient=?, body=?",[
    data.get('sender') or message['sender'],
    data.get('recipient') or message['recipient'],
    data.get('body') or message['body'],
  ])
  db.commit()

  return jsonify(message='message successfully updated')

@app.route('/messages', methods=['GET'])
@token_auth.login_required
def get_messages():
  users = request.args.get('users')
  if not users:
    return error_response(400, 'must provide users')

  sender, recipient = users.split(',')
  page = request.args.get('page', 1, type=int) or 1
  perpage = request.args.get('perpage', 1, type=int) or 10

  messages = db_util.query_db(
    "select \
      messages.id as 'message_id', \
      sender.username as 'from', \
      recipient.username as 'to', \
      messages.body as message \
    from messages\
    inner join users sender on sender.id = messages.sender\
    inner join users recipient on recipient.id = messages.recipient\
    where (messages.recipient=? and messages.sender=?) \
    or (messages.recipient=? and messages.sender=?)\
    limit {:d} offset {:d}".format(perpage, (page - 1) * perpage),
    [recipient, sender, sender, recipient])

  return jsonify(messages=messages, count=len(messages))

def error_response(status_code, message=None):
  payload = {
      'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')
  }
  if message: payload['message'] = message
  response = jsonify(payload)
  response.status_code = status_code
  return response

