DROP TABLE IF EXISTS users;
CREATE TABLE users
(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  username VARCHAR(64) NOT NULL UNIQUE,
  password_hash VARCHAR(128) NOT NULL UNIQUE,
  token VARCHAR(32),
  token_expires VARCHAR(32)
);

DROP TABLE IF EXISTS messages;
CREATE TABLE messages
(
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  recipient INTEGER,
  sender INTEGER,
  body VARCHAR
);
