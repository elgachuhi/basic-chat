from werkzeug.security import generate_password_hash
import sqlite3
from flask import g

from app import app

def dict_factory(cursor, row):
  d = {}
  for idx, col in enumerate(cursor.description):
    d[col[0]] = row[idx]
  return d

def connect_db():
  rv = sqlite3.connect(app.config['DATABASE'])
  rv.row_factory = dict_factory
  return rv

def get_db():
  if not hasattr(g, 'sqlite_db'):
    g.sqlite_db = connect_db()
  return g.sqlite_db

def query_db(query, args=()):
  cur = get_db().execute(query, args)
  rv = cur.fetchall()
  cur.close()
  return rv

def load_admin_user():
  db = get_db()
  db.execute('insert into users (username, password_hash) values(?, ?)',[
    app.config['ADMINUSERNAME'],
    generate_password_hash(app.config['ADMINPASS'])
  ])
  db.commit()

def init_db():
  db = get_db()
  with app.open_resource('db/sql/schema.sql', mode='r') as f:
    db.cursor().executescript(f.read())
  db.commit()
  load_admin_user()

@app.cli.command('initdb')
def initdb_command():
  init_db()
