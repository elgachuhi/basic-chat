from flask import g
import os
import base64
import db_util
from werkzeug.security import generate_password_hash, check_password_hash
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth
from flask import jsonify
from werkzeug.http import HTTP_STATUS_CODES

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth()


@basic_auth.verify_password
def verify_password(username, password):
  db = db_util.get_db()
  cur = db.execute('select *from users where username=?', [username])
  user = cur.fetchone()
  g.current_user = user
  if user is None:
    return False
  return check_password_hash(user['password_hash'], password)

@basic_auth.error_handler
def basic_auth_error():
  status_code = 401
  payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
  if message:
      payload['message'] = message
  response = jsonify(payload)
  response.status_code = status_code
  return response

@token_auth.verify_token
def verify_token(token):
  db = db_util.get_db()
  cur = db.execute("select token from users where token=?", [token])
  user = cur.fetchone()
  return user is not None

def gen_token():
  return base64.b64encode(os.urandom(32)).decode('utf-8')
