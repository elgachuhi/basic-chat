import os
import tempfile
import pytest
from flask import request
from app import app, db_util
from werkzeug.security import generate_password_hash
import base64

@pytest.fixture
def client():
  db_fd, app.config['DATABASE'] = tempfile.mkstemp()
  app.config['TESTING'] = True
  client = app.test_client()

  with app.app_context():
    db_util.init_db()
    db = db_util.get_db()
    db.execute("insert into users (username, password_hash) values(?, ?)",
      ['test_user', generate_password_hash('password')])
    db.execute("insert into users (username, password_hash) values(?, ?)",
      ['test_user-2', generate_password_hash('password-2')])
    db.execute('insert into messages (sender, recipient, body) values(?, ?, ?)',
      [1, 2, 'hello'])
    db.commit()

  yield client

  os.close(db_fd)
  os.unlink(app.config['DATABASE'])

class TokenAuth():
  def __init__(self, client):
    self._client = client
    self.token = self._get_token()

  def _get_token(self):
    res = self._client.open('/authenticate',
      method='POST',
      headers={
        'authorization': 'basic '+base64.b64encode("test_user:password")
    })
    data = res.get_json()
    return data['token']

@pytest.fixture
def auth(client):
  return TokenAuth(client)

def test_token_auth(auth):
  assert(auth.token != "" or (auth.token is not None))

def test_create_users(client, auth):
  data = client.post('/users',
    json={'username': 'mango-head', 'password': 'password'},
    headers={ 'Authorization': 'Bearer '+ auth.token}).get_json()
  assert(data['message'] == 'user successfully created')

def test_create_duplicate_users(client, auth):
  data = client.post('/users',
    json={'username': 'test_user', 'password': 'password'},
    headers={ 'Authorization': 'Bearer '+ auth.token}).get_json()
  assert(data['message'] == 'username has been taken')

def test_create_users_with_no_username(client, auth):
  data = client.post('/users',
    json={'password': 'password'},
    headers={ 'Authorization': 'Bearer '+ auth.token}).get_json()
  assert(data['message'] == 'No username or password')

def test_create_users_with_bad_token(client, auth):
  res = client.post('/users',
    json={'username': 'avocado-head', 'password': 'password'},
    headers={ 'Authorization': 'Bearer shdsh'})

  assert(res.status == '401 UNAUTHORIZED')

def test_update_users(client, auth):
  data = client.put('/users/1',
    json={'username': 'updated_test_user'},
    headers={'Authorization': 'Bearer '+ auth.token}).get_json()

  with app.app_context():
    db = db_util.get_db()
    cur = db.execute('select username from users where id=1')
    user = cur.fetchone()

  assert(user['username'] == 'updated_test_user')

def test_create_messages(client, auth):
  data = client.post('/messages',
    json={'sender': 1, 'recipient': 2, 'body': 'hello'},
    headers={'Authorization': 'Bearer '+ auth.token}).get_json()
  assert(data['message'] == "message created successfully")

def test_update_messages(client, auth):
  client.put('/messages/1',
    json={'sender': 1, 'recipient': 2, 'body': 'updated hello'},
    headers={ 'Authorization': 'Bearer '+ auth.token})

  with app.app_context():
    db = db_util.get_db()
    cur = db.execute('select body from messages where id=1')
    message = cur.fetchone()
  assert(message['body'] == 'updated hello')

def test_create_messages_with_no_sender(client, auth):
  data = client.post('/messages',
    json={'recipient': 2, 'body': 'hello'},
    headers={ 'Authorization': 'Bearer '+ auth.token }).get_json()
  assert(data['message'] == 'must provide body, sender and recipient')

def test_create_messages_with_no_receiver(client, auth):
  data = client.post('/messages',
    json={'sender': 2, 'body': 'hello'},
    headers={ 'Authorization': 'Bearer '+ auth.token}).get_json()
  assert(data['message'] == 'must provide body, sender and recipient')

def test_fetching_messages(client, auth):
  data = client.get('/messages', json={},
    headers={ 'Authorization': 'Bearer '+ auth.token}).get_json()
  assert('messages' in data)
